var express = require('express');
var router = express.Router();

/* GET xss page. */
router.get('/:xss', function(req, res, next) {
  res.send(req.params.xss);
});

module.exports = router;
