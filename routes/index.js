var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if(!process.env.CI && Math.random() < 0.5){
    res.status(500).send('Oops.')
  } else {
    res.render('index', { title: 'Express' });
  }
});

module.exports = router;
